package ru.t1.kharitonova.tm.exception.user;

import ru.t1.kharitonova.tm.exception.AbstractException;

public class RoleEmptyException extends AbstractException {

    public RoleEmptyException() {
        super("Error! Role is empty.");
    }

}
